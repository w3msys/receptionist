import Vue from 'vue';
import Router from 'vue-router';
import Login from './../components/login.vue';
import ResetPassword from './../components/reset_password.vue';
import Layout from './../components/layouts/index.vue';
import Dashboard from './../components/dashboard.vue';
import Booking from './../components/booking.vue';
import NewBooking from './../components/newbooking'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      component: Login,
      meta: {title: 'Login'}
    },
    {
      name: 'resetpassword',
      path: '/resetpassword',
      component: ResetPassword,
      meta: {title: 'Reset Password'}
    },
    {
      name: 'auth',
      path: '/user',
      component: Layout,
      children: [
        {
          name: 'dashboard',
          path: 'dashboard',
          component: Dashboard,
          meta: {title: 'Dashboard'}
        },
        {
          name: 'booking',
          path: 'booking',
          component: Booking,
          meta: {title: 'Booking'}
        },
        {
          name: 'newbooking',
          path: 'newbooking',
          component: NewBooking,
          meta: {title: 'New Booking'}
        }
      ]
    }
  ]
})

router.afterEach((to, from) => {
  const [title] = document.getElementsByTagName('title');
  title.innerText = to.meta.title ? `${to.meta.title} | Pupe` : 'Page not found Pupe'
})


export default router;